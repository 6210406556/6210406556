##สิ่งที่ทำในแต่ละcommit
ครังที่1-5 88eba70,a46fcf3,8fb5b3c,0bf951a,86b1def เอาmavenใส่เข้าไปในproject

ครั้งที่6 c8b40f8 สร้างหน้าguiของส่วนต่างๆในโปรแกรมให้สามารถนำไปพัฒนาต่อของcodeได้


ครั้งที่7 9f65b24 พยายามทำModelให้เป็นPolymorhpแต่ทำไม่ได้


ครั้งที่8 c4cf269 สร้างcsvมาเก็บข้อมูลของaccountและนำไปแสดงบนtableview


ครั้งที่9 ทำระบบการlogin featureการแบน การเพิ่มstaff การเปลี่ยนpassword(ยังไม่สมบูรณ์)


ครั้งที่10-19 	
fd1488e, cebc209, 34d5c7f,	81db331,10ef320,1ed205e,6f47362,5dd62ee,cce6821,0756568เพิ่มฟีเจอร์ และ การแก้บัคในโปรแกรมที่พบ


ครั้งที่20 e98c5a7 เพิ่มการฟีเจอร์แสดงจดหมายที่ห้องนั้นได้รับเท่านั้นในหน้าของuser



ครังที่ 21 dd9a000 ลองbuild jarไฟล์มาใช้ทดสอบ


##แก้ไขครั้งที่1
แก้ไขครั้งที่1 แก้ให้แสดงอาคารในตารางแสดงลำดับห้องทั้งหมด และหมายเลขห้องจะมีfotmat เป็น 101 เลขหน้าสุดคือชั้นของห้อง และ 01 คือหมายเลขของห้องในชั้นนั้น
          
          
แก้ให้หน้าของเปลี่ยนpasswordจากTextfieldเป็นpasswordField แก้ไขปัญหาการแบน และบัคบางอย่างในการadd room
          
          
ชี้แจงในcommentว่าห้องdoubleทำไมเพิ่มคนเข้าไป2คนไม่ได้ จากที่ลองtestแล้วได้นะครับแต่ว่าตัวโปรแกรมจะให้เพิ่มผู้พักอาศัยได้ทีละคนไม่สามารถเพิ่มทีเดียวสองคนได้


หน้าคู่มือการใช่้จะมีloadข้อมูลขึ้นมาช้า


commitในbranchหายแต่สามารถดูได้ในread meด้านบน



##directory ใน repository เก็บอะไรไว้

data->User.csv เอาไว้เก็บแอคเค้าทั้งหมดที่มีอยู่


data->Storage.csv เอาไว้เก็บของที่ส่งมาถึง


src->main->java->mailbox->controller เอาไว้เก็บcontrollerทั้งหมดของfxmlในอนาคตจะมีการรวมcontrollบางส่วนไว้ด้วย และจะจัดแบ่งหมวดหมู่


src->main->java->mailbox->controller->AddStaffController เป็นcontrollerสำหับการเพิ่มstaffเข้าไปในระบบซึ่งควบคุมaddOfficerPage.fxml


src->main->java->mailbox->controller->AdminController เป็นหน้าควบคุมของหน้าต่างadminInterface.fxml


src->main->java->mailbox->controller->ChangePasswordController เป็นหน้าควบคุมของหน้าต่างการเปลี่ยนpasswordของchangePasswordPage.fxml


+src->main->java->mailbox->controller->HelpPageController เป็นหน้าควบคุมของหน้าต่างของหน้าให้คำช่วยเหลือHelpPage.fxml


src->main->java->mailbox->controller-> RegisterController เป็นหน้าควบคุมของหน้าต่างของหน้าregisterPage.fxmlเอาไว้สมัครบัญชีของผู้เข้าอาศัย


src->main->java->mailbox->controller-> StaffController เป็นหน้าควบคุมของหน้าต่างของหน้าstaffPage.fxml


src->main->java->mailbox->controller-> UserController เป็นหน้าควบคุมของหน้าต่างของหน้าuserInterface.fxml


src->main->java->mailbox->controller-> UserReceivedController เป็นหน้าควบคุมของหน้าต่างของหน้าuserReceiveInterface.fxml


src->main->java->mailbox->controller-> ViewHistoryController เป็นหน้าควบคุมของหน้าต่างของหน้าuserReceiveInterface.fxml


src->main->java->mailbox->controller->CreditController เป็นหน้าควบคุมของหน้าต่างแสดงผู้จัดทำ


src->main->java->mailbox->AccModel เก็บclass modelของaccountเอาไว้


src->main->java->mailbox->StorageModel เก็บclass modelของกล่องจดหมายเอาไว้


src->main->java->mailbox->Service เก็บไฟล์datasourceเอาไว้ในการอ่านและเขียนไฟล์


src->main->resources->font เก็บfontที่จะเอาไว้ใช้


src->main->resources->fxml เก็บfxmlไว้ทั้งหมด


src->main->resources->pic ที่เก็บรูปจากกันอัพโหลดของแต่ละuser


src->main->resources->stylesheet เก็บcssเอาไว้


src->main->java->mailbox->strategy  เก็บวิธีการหาชื่อและห้อง


.../images  เก็บรูปที่เพิ่มเข้าของทั้งuserและพัสดุ
 

.../app/images เก็บรูปของuserและพัสดุที่เพิ่มเข้ามา


.../app/data เป็นที่เก็บของcsv


.../app เป็นที่เก็บของโปรแกรม mail box


##การใช้งาน
กดติดตั้งfontในsrc->main->java->resources->fontท้งหมด
target->6210406556->src->main->6210406556-jar.jarแล้วดับเบิ้ลคลิกเปิดขึ้นมา
หากใช้งานไม่ได้ให้เปิดcommand prompt และพิม-jar.jar แล้วลากไฟล์6210406556-jar.jarเอาไปใส่


##การทดสอบ
(Admin)


Username:ADMIN


Password:1234


(Staff)


Username:STAFF


Password:1234


(residents)


Username:USER


Password:1234



