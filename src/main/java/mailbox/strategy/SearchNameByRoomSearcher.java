package mailbox.strategy;

import mailbox.model.building.Room;
import mailbox.model.storageModel.Letter;

public class SearchNameByRoomSearcher implements Searcher{
    private String nameSearch ;

    public void setNameSearch(String nameSearch) {
        this.nameSearch = nameSearch;
    }

    @Override
    public boolean isMatch(Letter letter) {
        return false;
    }

    @Override
    public boolean isMatchName(Room room) {
        return room.getResidentName().contains(nameSearch);
    }
}
