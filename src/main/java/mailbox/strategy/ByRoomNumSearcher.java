package mailbox.strategy;

import mailbox.model.building.Room;
import mailbox.model.storageModel.Letter;

public class ByRoomNumSearcher implements Searcher{
    private String roomNum;

    public void setRoomID(String roomNum) {
        this.roomNum = roomNum;
    }

    @Override
    public boolean isMatch(Letter letter) {
        return letter.getRoomNumber().equals(roomNum);
    }

    @Override
    public boolean isMatchName(Room room) {
        return false;
    }


}
