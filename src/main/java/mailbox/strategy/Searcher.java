package mailbox.strategy;

import mailbox.model.building.Room;
import mailbox.model.storageModel.Letter;

public interface Searcher {
    boolean isMatch(Letter letter);
    boolean isMatchName(Room room);
}
