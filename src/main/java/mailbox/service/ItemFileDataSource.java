package mailbox.service;


import mailbox.model.storageModel.Box;
import mailbox.model.storageModel.Document;
import mailbox.model.storageModel.Letter;
import mailbox.model.storageModel.Storage;

import java.io.*;

public class ItemFileDataSource {
    private final String fileDirectoryName;
    private final String letterFile ;
    //private String userFile;
    // private String staffFile;
    private Storage storage;

    public ItemFileDataSource(String fileDirectoryName) {
        this.fileDirectoryName = fileDirectoryName;
        this.letterFile = "Storage.csv";
        checkFileIsExisted(letterFile);

    }

    private void checkFileIsExisted(String fileName) {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData(String fileName) throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            if (data[0].trim().equals("Letter"))
            { Letter letter = new Letter(data[0].trim(),data[1].trim(),data[2].trim(), data[3].trim(),data[4].trim(),data[5].trim(),
                    data[6].trim(),data[7].trim(),data[8].trim(),data[9].trim(),data[10].trim(),data[11].trim());

                storage.addItem(letter);}
            else if(data[0].trim().equals("Doc")){
                Document document = new Document(data[0].trim(),data[1].trim(),data[2].trim(), data[3].trim(),data[4].trim(),data[5].trim(),
                        data[6].trim(),data[7].trim(),data[8].trim(),data[9].trim(),data[10].trim(),data[11].trim(),data[12].trim());

                storage.addItem(document);
            }
            else if (data[0].trim().equals("Box")){
                Box box = new Box(data[0].trim(),data[1].trim(),data[2].trim(), data[3].trim(),data[4].trim(),data[5].trim(),
                        data[6].trim(),data[7].trim(),data[8].trim(),data[9].trim(),data[10].trim(),data[11].trim(),data[12].trim(),data[13].trim());

                storage.addItem(box);
            }
        }
        reader.close();
    }




    public Storage getStorageData(String fileName) {
        try {
            storage = new Storage();
            readData(fileName);
        } catch (FileNotFoundException e) {
            System.err.println(this.fileDirectoryName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileDirectoryName);
        }
        return storage;
    }


    public void setLetterData(Storage storage) {
        String filePath = fileDirectoryName + File.separator + letterFile ;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Letter letter: storage.toList()) {
                if (letter.getIemType().equals("Letter")){
                String line = letter.getIemType() + ","
                        + letter.getReceiver() + ","
                        + letter.getRoomNumber() + ","
                        + letter.getSenderName()+","
                        + letter.getStaffName()+","
                        + letter.getTitle()+","
                        + letter.getDate()+","
                        + letter.getPic()+","
                        + letter.getSize()+","
                        + letter.getStatus()+","
                        + letter.getTakenDate()+","
                        + letter.getStaffTake();

                writer.append(line);
                writer.newLine();}
                else if (letter.getIemType().equals("Doc")){
                    String line = letter.getIemType() + ","
                            + letter.getReceiver() + ","
                            + letter.getRoomNumber() + ","
                            + letter.getSenderName()+","
                            + letter.getStaffName()+","
                            + letter.getTitle()+","
                            + letter.getDate()+","
                            + letter.getPic()+","
                            + letter.getSize()+","
                            + letter.getStatus()+","
                            + letter.getTakenDate()+","
                            + letter.getStaffTake()+","
                            + ((Document)letter).getPriority();

                    writer.append(line);
                    writer.newLine();
                }
                else if (letter.getIemType().equals("Box")){
                    String line = letter.getIemType() + ","
                            + letter.getReceiver() + ","
                            + letter.getRoomNumber() + ","
                            + letter.getSenderName()+","
                            + letter.getStaffName()+","
                            + letter.getTitle()+","
                            + letter.getDate()+","
                            + letter.getPic()+","
                            + letter.getSize()+","
                            + letter.getStatus()+","
                            + letter.getTakenDate()+","
                            + letter.getStaffTake()+","
                            + ((Box)letter).getDeliveryService()+","
                            + ((Box)letter).getTrackingNumber();

                    writer.append(line);
                    writer.newLine();
                }
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }



}
