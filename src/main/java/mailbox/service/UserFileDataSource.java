package mailbox.service;


import mailbox.model.accModel.Account;
import mailbox.model.accModel.AccountManage;

import java.io.*;

public class UserFileDataSource {

    private String fileDirectoryName;
    private String adminFile;
    //    private String userFile;
//    private String staffFile;
    private AccountManage accounts;

    public UserFileDataSource(String fileDirectoryName) {
        this.fileDirectoryName = fileDirectoryName;
        this.adminFile = "User.csv";
        checkFileIsExisted(adminFile);

    }

    private void checkFileIsExisted(String fileName) {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData(String fileName) throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Account account = new Account(data[0].trim(), data[1].trim(), data[2].trim(),Integer.parseInt(data[3].trim()) , data[4].trim(), data[5].trim(), data[6].trim(), data[7].trim());

            if (data[8].trim().equals("ban")) {
                account.setBanAccStatus();
            }
            accounts.addAccount(account);


        }
        reader.close();
    }


    public AccountManage getAccountsData(String fileName) {
        try {
            accounts = new AccountManage();
            readData(fileName);
        } catch (FileNotFoundException e) {
            System.err.println(this.fileDirectoryName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileDirectoryName);
        }
        return accounts;
    }

    public void setStaffData(AccountManage accounts) {
        String filePath = fileDirectoryName + File.separator + adminFile;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Account account : accounts.toList()) {
                String line = account.getAccName() + ","
                        + account.getPassword() + ","
                        + account.getName() + ","
                        + account.getAttempt() + ","
                        + account.getOnlineDate() + ","
                        + account.getFilePath() + ","
                        + account.getAccType() + ","
                        + account.getRoomNumber() + ","
                        + account.getAccStatus()
                        ;

                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }


}
