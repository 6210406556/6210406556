package mailbox.service;


import mailbox.model.building.Building;
import mailbox.model.building.Room;

import java.io.*;

public class RoomFileDataSource {

    private String fileDirectoryName;
    private String roomFile;

    private Building rooms;

    public RoomFileDataSource(String fileDirectoryName) {
        this.fileDirectoryName = fileDirectoryName;
        this.roomFile = "Room.csv";
        checkFileIsExisted(roomFile);

    }

    private void checkFileIsExisted(String fileName) {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData(String fileName) throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Room room = new Room(data[0].trim(),data[1].trim(),data[2].trim(),data[3].trim(),data[4].trim(),data[5].trim());

            rooms.addRoom(room);


        }
        reader.close();
    }




    public Building getRoomData(String fileName) {
        try {
            rooms = new Building();
            readData(fileName);
        } catch (FileNotFoundException e) {
            System.err.println(this.fileDirectoryName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileDirectoryName);
        }
        return rooms;
    }

    public void setRoomData(Building building) {
        String filePath = fileDirectoryName + File.separator + roomFile ;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Room room: rooms.toList()) {
                String line = room.getTower() + ","
                        + room.getRoomNum() + ","
                        + room.getFloor() + ","
                        + room.getRoomType()+","
                        + room.getResidentName()+","
                        + room.getSecondResidentName();

                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }



}
