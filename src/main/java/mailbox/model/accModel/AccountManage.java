package mailbox.model.accModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class AccountManage {
    private ArrayList<Account> accounts;
    private Account currentAccount;
    public AccountManage(){accounts = new ArrayList<>();}
    public void addAccount(Account acc){
        accounts.add(acc);
    }
    public boolean checkPassword(String accName,String password){
        for (Account acc : accounts){
            if (acc.getPassword().equals(password)&&acc.getAccName().equals(accName)){
                currentAccount = acc;
                return true;
            }
        }
        currentAccount = null;
        return false;
    }


    public Account getCurrentAccount() {
        return currentAccount;
    }

    public ArrayList<Account> toList() {
        return accounts;
    }
    public ArrayList<Account> toStaffList(){
        ArrayList<Account> staffAccountList = new ArrayList<>();
        for (Account acc:accounts){
            if (acc.getAccType().equals("s")){
                staffAccountList.add(acc);
            }

        }
        Collections.sort(staffAccountList, new Comparator<Account>() {
            @Override
            public int compare(Account o1, Account o2) {
                return o2.getOnlineDate().compareTo(o1.getOnlineDate());
            }


        });
        return staffAccountList;
    }
}

