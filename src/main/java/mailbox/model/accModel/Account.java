package mailbox.model.accModel;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Account {
    private String accName;
    private String password;
    private String name;
    private int attempt;
    private String onlineDate;
    private String filePath;
    private String accType;
    private String RoomNumber;
    private String accStatus;


    public Account(String accName, String password, String name, int attempt, String onlineDate, String filePath, String accType, String RoomNumber) {
        this.accName = accName;
        this.password = password;
        this.name = name;
        this.attempt = attempt;
        this.onlineDate = onlineDate;
        this.filePath = filePath;
        this.accType = accType;
        this.accStatus = "normal";
        this.RoomNumber = RoomNumber;
    }

    public String getFilePath() {
        return filePath;
    }

    public String getAccName() {

        return accName;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getOnlineDate() {
        return onlineDate;
    }

    public String getRoomNumber() {
        return RoomNumber;
    }

    public String getAccType() {
        return accType;
    }

    public int getAttempt() {
        return attempt;
    }

    public void setAttempt(int attempt) {
        this.attempt = attempt;
    }


    public void setPassword(String password) {
        this.password = password;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setBanAccStatus() {
        this.accStatus = "ban";
    }

    public void setUnBanAccStatus() {
        this.accStatus = "normal";
    }
    public boolean isU(){
        if (accType.equals("u")){
            return true;
        }
        return false;
    }
    public boolean isAdmin(){if (accType.equals("a")){
        return true;
    }
        return false;}
    public boolean isStaff(){if (accType.equals("s")){
        return true;
    }
        return false;}
    public String getAccStatus() {
        return accStatus;
    }

    public void setRoomNumber(String roomNumber) {
        RoomNumber = roomNumber;
    }

    public void setDate() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String strDate = formatter.format(date);
        this.onlineDate = strDate;

    }
}

