package mailbox.model.storageModel;

public class Document extends Letter  {
    private String priority;

    public Document(String iemType, String receiver, String roomNumber, String senderName, String staffName, String title, String date, String pic, String size, String status, String takenDate, String staffTake, String priority) {
        super(iemType, receiver, roomNumber, senderName, staffName, title, date, pic, size, status, takenDate, staffTake);
        this.priority = priority;
    }

    public String getPriority() {
        return priority;
    }


}
