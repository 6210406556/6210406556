package mailbox.model.storageModel;

public class Letter {
    private String iemType;
    private String receiver;
    private String roomNumber;
    private String senderName;
    private String staffName;
    private String title;
    private String date;
    private String pic;
    private String size;
    private String status;
    private String takenDate;
    private String staffTake;
    public Letter(String iemType, String receiver, String roomNumber, String senderName, String staffName
            , String title, String date, String pic, String size, String status,String takenDate,String staffTake) {
        this.iemType = iemType;
        this.receiver = receiver;
        this.roomNumber = roomNumber;
        this.senderName = senderName;
        this.staffName = staffName;
        this.title = title;
        this.date = date;
        this.pic = pic;
        this.size = size;
        this.status = status;
        this.takenDate = takenDate;
        this.staffTake = staffTake;
    }

    public String getIemType() {
        return iemType;
    }

    public void setIemType(String iemType) {
        this.iemType = iemType;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTakenDate() {
        return takenDate;
    }

    public String getStaffTake() {
        return staffTake;
    }

    public void setStaffTake(String staffTake) {
        this.staffTake = staffTake;
    }

    public void setTakenDate(String takenDate) {
        this.takenDate = takenDate;
    }

}




