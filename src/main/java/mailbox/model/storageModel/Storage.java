package mailbox.model.storageModel;

import mailbox.strategy.Searcher;

import java.util.ArrayList;

public class Storage {
    private ArrayList<Letter> letters;

    public Storage() {
        letters = new ArrayList<>();
    }

    public void addItem(Letter item) {
        letters.add(item);
    }


    public ArrayList<Letter> toList() {
        return letters;
    }

    public ArrayList<Letter> toNotReceiveList() {
        ArrayList<Letter> itemList = new ArrayList<>();
        for (Letter letter : letters) {
            if (letter.getStatus().equals("nr")) {
                itemList.add(letter);
            }

        }
        return itemList;
    }
    public ArrayList<Letter> toReceiveList() {
        ArrayList<Letter> takeList = new ArrayList<>();
        for (Letter letter : letters) {
            if (letter.getStatus().equals("r")) {
                takeList.add(letter);
            }

        }
        return takeList;
    }


    public ArrayList<Letter> search(Searcher searcher) {
        ArrayList<Letter> newList = new ArrayList<>();
        for (Letter letter: toNotReceiveList()) {
            if (searcher.isMatch(letter)) {
                newList.add(letter);
            }
        }
        return newList;
    }
    public ArrayList<Letter> search2(Searcher searcher) {
        ArrayList<Letter> newList = new ArrayList<>();
        for (Letter letter: toReceiveList()) {
            if (searcher.isMatch(letter)) {
                newList.add(letter);
            }
        }
        return newList;
    }

}
