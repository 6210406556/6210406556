package mailbox.model.storageModel;

public class Box extends Letter{
    private String deliveryService;
    private String trackingNumber;

    public Box(String iemType, String receiver, String roomNumber, String senderName, String staffName, String title, String date, String pic, String size, String status, String takenDate, String staffTake, String deliveryService, String trackingNumber) {
        super(iemType, receiver, roomNumber, senderName, staffName, title, date, pic, size, status, takenDate, staffTake);
        this.deliveryService = deliveryService;
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }
    public String getDeliveryService() {
        return deliveryService;
    }

    public void setDeliveryService(String deliveryService) {
        this.deliveryService = deliveryService;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }
}
