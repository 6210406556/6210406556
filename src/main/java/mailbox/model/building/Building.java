package mailbox.model.building;

import mailbox.strategy.Searcher;

import java.util.ArrayList;

public class Building {
    private ArrayList<Room> rooms;

    public Building(){rooms = new ArrayList<>();}
    public void addRoom(Room room){
        rooms.add(room);
    }



    public ArrayList<Room> toList() {
        return rooms;
    }


    public ArrayList<Room> search(Searcher searcher) {
        ArrayList<Room> newList = new ArrayList<>();
        for (Room room: rooms) {
            if (searcher.isMatchName(room)) {
                newList.add(room);
            }
        }
        return newList;
    }

    public int count() {
        return rooms.size();
    }
}
