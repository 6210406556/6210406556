package mailbox.model.building;

public class Room {
    private String tower;
    private String roomNum;
    private String floor;
    private String roomType;
    private String residentName;
    private String secondResidentName;

    public Room(String tower,String roomNum, String floor, String roomType, String residentName,String secondResidentName) {
        this.tower = tower;
        this.roomNum = roomNum;
        this.floor = floor;
        this.roomType = roomType;
        this.residentName = residentName;
        this.secondResidentName = secondResidentName;
    }

    public String getSecondResidentName() {
        return secondResidentName;
    }

    public String getTower() {
        return tower;
    }

    public void setTower(String tower) {
        this.tower = tower;
    }

    public void setSecondResidentName(String secondResidentName) {
        this.secondResidentName = secondResidentName;
    }

    public String getRoomNum() {
        return roomNum;
    }

    public void setRoomNum(String roomNum) {
        this.roomNum = roomNum;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getResidentName() {
        return residentName;
    }

    public void setResidentName(String residentName) {
        this.residentName = residentName;
    }
}

