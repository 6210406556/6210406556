package mailbox.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import mailbox.model.accModel.Account;
import mailbox.model.building.Building;
import mailbox.model.storageModel.Letter;
import mailbox.model.storageModel.Storage;
import mailbox.service.ItemFileDataSource;
import mailbox.service.RoomFileDataSource;
import mailbox.service.StringConfiguration;
import mailbox.strategy.ByRoomNumSearcher;
import mailbox.strategy.Searcher;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ViewHistoryController {
    private ItemFileDataSource itemFileDataSource;
    private Storage storage;
    private RoomFileDataSource roomFileDataSource;
    private Building building;
    private Account account;
    private ObservableList<Letter> mailboxObservableList;
    private Letter selectedItem;
    private Searcher searcher;
    @FXML private TextField searchText;
    @FXML
    private Label currentLabel,staff,date,staffTake,dateTake;
    @FXML
    private ImageView profilePic, itemPic;
    @FXML
    TableView mailboxTableView;
    @FXML
    Button backBtn,changePasswordBtn;
    @FXML
    ChoiceBox<String> searchOptions;
    @FXML
    public void initialize() {
        itemFileDataSource = new ItemFileDataSource("data");
        storage = itemFileDataSource.getStorageData("Storage.csv");
        roomFileDataSource = new RoomFileDataSource("data");
        building = roomFileDataSource.getRoomData("Room.csv");
        showMailboxData();
        initSearchOption();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentLabel.setText("Name:" + account.getAccName());
                profilePic.setImage(new Image(new File(account.getFilePath()).toURI().toString()));
                mailboxTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        showData((Letter) newValue);

                    }
                });
            }
        });
    }

    private void initSearchOption() {
        String[] options = {
                "หาจากห้อง",
                "--Clear--"
        };

        for (int i = 0; i < options.length; i++) {
            searchOptions.getItems().add(options[i]);
        }

        searchOptions.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals(options[0])) {
                optionByRoomNumSearcher();
            } else if (newValue.equals(options[1])) {
                optionClear();
            }

        });
    }

    private void optionByRoomNumSearcher() {
        searcher = new ByRoomNumSearcher();
        searchText.setDisable(false);
        searchText.textProperty().addListener((observable, oldValue, newValue) -> {
            String roomNum = newValue;
            ((ByRoomNumSearcher)searcher).setRoomID(roomNum);
        });
    }
    private void optionClear() {
        searchText.setDisable(true);
        searcher = null;
        searchText.clear();
        updateListView(storage.toReceiveList());

    }
    private void showMailboxData() {
        mailboxObservableList = FXCollections.observableArrayList(storage.toReceiveList());
        mailboxTableView.setItems(mailboxObservableList);
        ArrayList<StringConfiguration> config = new ArrayList<>();
        config.add(new StringConfiguration("title:ประเภท", "field:iemType"));
        config.add(new StringConfiguration("title:หัวข้อ", "field:title"));
        config.add(new StringConfiguration("title:ถึง", "field:receiver"));
        config.add(new StringConfiguration("title:จาก", "field:senderName"));
        config.add(new StringConfiguration("title:หมายเลขห้อง", "field:roomNumber"));
        config.add(new StringConfiguration("title:วันที่จดหมายเข้าส่วนกลาง", "field:date"));


        for (StringConfiguration conf : config) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            col.prefWidthProperty().bind(mailboxTableView.widthProperty().divide(6));
            mailboxTableView.getColumns().add(col);
        }
    }
    private void showData(Letter letter) {
        selectedItem = letter;
        staff.setText(letter.getStaffName());
        date.setText(letter.getDate());
        staffTake.setText(letter.getStaffTake());
        dateTake.setText(letter.getTakenDate());
        itemPic.setImage(new Image(new File(selectedItem.getPic()).toURI().toString()));
    }
    @FXML
    public void handleChangePasswordBtn(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/changePasswordPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(), 1024, 768));

        ChangePasswordController changePasswordController = loader.getController();
        changePasswordController.setAccountManage(account);
        stage.show();
    }
    @FXML
    public void handleBackBtn(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/staffPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(), 1024, 768));

        StaffController staffController = loader.getController();
        staffController.setAccountManage(account);
        stage.show();
    }

    private void updateListView(ArrayList<Letter> list) {
        mailboxTableView.getItems().clear();
        for (Letter letter : list) {
            mailboxTableView.getItems().add(letter);
        }
    }

    @FXML
    public void handleUpdateButton(ActionEvent event) {
        if (searcher != null) {
            updateListView(storage.search2(searcher));
        } else {
            updateListView(storage.toReceiveList());
        }
    }
    public void setAccountManage(Account account) {
        this.account = account;
    }
}
