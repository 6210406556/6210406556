package mailbox.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;


public class HelpPageController {
    @FXML private VBox vBox;
    @FXML public void initialize(){
        Platform.runLater(()->{
            for (int i = 1 ;i <= 16;i++){
                String imgPath = "/helpPic/"+i+".png";
                ImageView image = new ImageView(new Image(imgPath));
                vBox.getChildren().add(image);

            }
        });
    }
    @FXML public void handleBackBthOnAction(ActionEvent event) throws IOException {
            Button register = (Button) event.getSource();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/loginPage.fxml"));
            Stage stage = (Stage) register.getScene().getWindow();
            stage.setScene(new Scene(loader.load(),1024,768));}

}
