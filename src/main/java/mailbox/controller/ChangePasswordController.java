package mailbox.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import mailbox.model.accModel.Account;
import mailbox.model.accModel.AccountManage;
import mailbox.service.UserFileDataSource;

import javax.swing.text.html.ImageView;
import java.io.IOException;

public class    ChangePasswordController {
    @FXML private Button backBtn;
    @FXML private Button okBtn;
    @FXML private PasswordField oldPassword;
    @FXML private PasswordField newPassword;
    @FXML private PasswordField conPassword;
    @FXML private ImageView profile;
    private AccountManage accountManage;
    private Account account;
    private UserFileDataSource dataSource;

    @FXML public void initialize(){
        Platform.runLater(()->{
            dataSource = new UserFileDataSource("data");
            accountManage = new AccountManage();
            accountManage = dataSource.getAccountsData("User.csv");
        });
    }
    @FXML public void handleBackBthOnAction(ActionEvent event) throws IOException {
        if (account.getAccType().equals("a")){
            Button register = (Button) event.getSource();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/adminInterface.fxml"));
            Stage stage = (Stage) register.getScene().getWindow();
            stage.setScene(new Scene(loader.load(),1024,768));
            AdminController adminController= loader.getController();
            adminController.setAccountManage(account);
        }
        else if(account.getAccType().equals("s")){
            Button register = (Button) event.getSource();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/staffPage.fxml"));
            Stage stage = (Stage) register.getScene().getWindow();
            stage.setScene(new Scene(loader.load(),1024,768));
            StaffController staffController= loader.getController();
            staffController.setAccountManage(account);
        }else if (account.getAccType().equals("u")){
            System.out.println(account.getAccName());
            Button register = (Button) event.getSource();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/userInterface.fxml"));
            Stage stage = (Stage) register.getScene().getWindow();
            stage.setScene(new Scene(loader.load(),1024,768));
            UserController userController= loader.getController();
            userController.setAccountManage(account);}

    }
    @FXML public void handleOkBtnOnAction(ActionEvent event) throws IOException {
        for (Account acc:accountManage.toList()){
            if (acc.getAccName().equals(account.getAccName())){
                if (oldPassword.getText().equals(acc.getPassword())&&newPassword.getText().equals(conPassword.getText())){
                    acc.setPassword(newPassword.getText());
                    dataSource.setStaffData(accountManage);
                    Button con = (Button) event.getSource();
                    if (account.isAdmin()){
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/adminInterface.fxml"));
                        Stage stage = (Stage) con.getScene().getWindow();
                        stage.setScene(new Scene(loader.load(),1024,768));
                        AdminController adminController= loader.getController();
                        adminController.setAccountManage(account);
                    }
                    else if(account.isStaff()){
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/staffPage.fxml"));
                        Stage stage = (Stage) con.getScene().getWindow();
                        stage.setScene(new Scene(loader.load(),1024,768));
                        StaffController staffController= loader.getController();
                        staffController.setAccountManage(account);
                    }else if (account.isU()){
                        Button register = (Button) event.getSource();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/userInterface.fxml"));
                        Stage stage = (Stage) register.getScene().getWindow();
                        stage.setScene(new Scene(loader.load(),1024,768));
                        UserController userController= loader.getController();
                        userController.setAccountManage(account);}
                break;
                }

                else if (oldPassword.getText().equals("")||newPassword.getText().equals("")||conPassword.getText().equals("")){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Wrong");
                    alert.setContentText("กรุณากรอกข้อมูลให้ครบ");
                    alert.setHeaderText(null);
                    alert.showAndWait();
                    break;
                }else{
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Wrong");
                    alert.setContentText("รหัสผ่านไม่ถูกต้อง");
                    alert.setHeaderText(null);
                    alert.showAndWait();
                    break;
                }
            }
        }
    }
    public void setAccountManage(Account account) {
        this.account= account;
    }
}
