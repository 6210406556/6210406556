package mailbox.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import mailbox.model.accModel.Account;
import mailbox.model.accModel.AccountManage;
import mailbox.service.UserFileDataSource;
import mailbox.service.StringConfiguration;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class AdminController {

    private AccountManage accounts;
    private Account account;
    private Account selectedAccount;
    private ObservableList<Account> accountObservableList;
    private UserFileDataSource dataSource;
    @FXML private Button logoutBtn;
    @FXML private Label currentLabel;
    @FXML private Label idLabel;
    @FXML private Button banBtn;
    @FXML private Button unBanBtn;
    @FXML private Button changePasswordBtn;
    @FXML private Button addStaffBtn;
    @FXML private ImageView profilePic;
    @FXML private TableView<Account> accountTableView;

    @FXML public void initialize(){
        dataSource = new UserFileDataSource("data");
        accounts = dataSource.getAccountsData("User.csv");

        showAccountData();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentLabel.setText("Name:"+ account.getName());
                idLabel.setText("UserName:"+account.getAccName());
                accountTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->{
                    if (newValue != null){
                        showData(newValue);

                    }
                });
            }
        });
    }

    private void showAccountData(){
        accountObservableList = FXCollections.observableArrayList(accounts.toStaffList());
        accountTableView.setItems(accountObservableList);
        ArrayList<StringConfiguration> config = new ArrayList<>();
        config.add(new StringConfiguration("title:ชื่อผู้ใช้", "field:accName"));
        config.add(new StringConfiguration("title:ชื่อ", "field:name"));
        config.add(new StringConfiguration("title:สถานะ","field:accStatus"));
        config.add(new StringConfiguration("title:เข้าใช้ครั้งล่าสุด","field:onlineDate"));
        config.add(new StringConfiguration("title:พยายามเข้า","field:attempt"));

        for (StringConfiguration conf: config) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            col.prefWidthProperty().bind(accountTableView.widthProperty().divide(5));
            accountTableView.getColumns().add(col);
        }
    }
    @FXML public void handleLogoutBtnOnAction(ActionEvent event) throws IOException{
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/loginPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(),1024,768));

        stage.show();
    }
    @FXML public  void handleChangePasswordBtn(ActionEvent event) throws IOException{
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/changePasswordPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(),1024,768));

        ChangePasswordController changePasswordController = loader.getController();
        changePasswordController.setAccountManage(account);
        stage.show();
    }
    @FXML public void handleBanBtn(ActionEvent event) {
        if (selectedAccount.getAccStatus().equals("normal")){
            selectedAccount.setBanAccStatus();
        }
        else{
            selectedAccount.setUnBanAccStatus();
            selectedAccount.setAttempt(0);
        }
        accountTableView.refresh();
        accountTableView.getSelectionModel().clearSelection();
        dataSource.setStaffData(accounts);

    }



    @FXML public void handleAddStaffBtnOnAction(ActionEvent event) throws IOException{

        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/addOfficerPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(),1024,768));

        AddStaffController addStaffController = loader.getController();
        addStaffController.setAccountManage(account);
        stage.show();
    }
    private void showData(Account account){
        selectedAccount = account;
        currentLabel.setText("ชื่อผู้ใช้: "+account.getAccName());
        idLabel.setText("ชื่อ: "+account.getName());
        if(selectedAccount.getFilePath().equals(null)){
            profilePic.setImage(new Image("/pic/images.png"));
        }
        else{
            profilePic.setImage(new Image(new File(selectedAccount.getFilePath()).toURI().toString()));}
    }
    public void setAccountManage(Account account) {
        this.account = account;
    }

    public ObservableList<Account> getAccountObservableList() {
        return accountObservableList;
    }


}