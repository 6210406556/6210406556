package mailbox.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import mailbox.model.accModel.Account;
import mailbox.model.building.Building;
import mailbox.model.storageModel.Box;
import mailbox.model.storageModel.Document;
import mailbox.model.storageModel.Letter;
import mailbox.model.storageModel.Storage;
import mailbox.service.ItemFileDataSource;
import mailbox.service.RoomFileDataSource;
import mailbox.service.StringConfiguration;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class UserController {
    @FXML
    private Button logoutBtn,backBtn,receivedBtn;
    @FXML
    private Button changePassword;
    @FXML
    private Button viewInfo;
    @FXML
    private Label name;
    @FXML
    private Label id, currentLabel;
    @FXML
    private ImageView profilePic, itemPic,itemPic2;
    @FXML
    private TableView<Letter> mailboxTableView,mailboxTableView2;
    @FXML private Pane docPane,boxPane;
    private Account account;
    private ObservableList<Letter> mailboxObservableList;
    private Storage storage;
    private Letter selectedItem;
    @FXML
    private Label  senderLabel,date,titleLabel,receiver,pio,trackNum,service;
    private ItemFileDataSource itemFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private Building building;

    public UserController() {
    }

    @FXML
    public void initialize() {
        itemFileDataSource = new ItemFileDataSource("data");
        storage = itemFileDataSource.getStorageData("Storage.csv");
        roomFileDataSource = new RoomFileDataSource("data");
        building = roomFileDataSource.getRoomData("Room.csv");
        boxPane.setVisible(false);
        docPane.setVisible(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                showMailboxData();
                currentLabel.setText("Name:" + account.getAccName());
                profilePic.setImage(new Image(new File(account.getFilePath()).toURI().toString()));
                mailboxTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        showData(newValue);

                    }
                });

            }
        });
    }
    private void showMailboxData() {
        ArrayList<Letter> newList = new ArrayList<>();
        for(Letter letter:storage.toNotReceiveList()){
                if (account.getRoomNumber().equals(letter.getRoomNumber())){
                    newList.add(letter);
                }
        }
        mailboxObservableList = FXCollections.observableArrayList(newList);
        mailboxTableView.setItems(mailboxObservableList);
        ArrayList<StringConfiguration> config = new ArrayList<>();
        config.add(new StringConfiguration("title:ประเภท", "field:iemType"));
        config.add(new StringConfiguration("title:หัวข้อ", "field:title"));
        config.add(new StringConfiguration("title:ถึง", "field:receiver"));
        config.add(new StringConfiguration("title:หมายเลขห้อง", "field:roomNumber"));
        config.add(new StringConfiguration("title:วันที่จดหมายเข้าส่วนกลาง", "field:date"));
        config.add(new StringConfiguration("title:จาก", "field:senderName"));

        for (StringConfiguration conf : config) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            col.prefWidthProperty().bind(mailboxTableView.widthProperty().divide(6));
            mailboxTableView.getColumns().add(col);
        }
    }


    private void showData(Letter letter) {
        selectedItem = letter;
        titleLabel.setText("หัวข้อ: "+letter.getTitle());
        receiver.setText("ถึง: "+letter.getReceiver());
        senderLabel.setText("จาก: "+letter.getSenderName());
        date.setText("เวลา:"+"\n"+letter.getDate());
        itemPic.setImage(new Image(new File(selectedItem.getPic()).toURI().toString()));
        if (selectedItem.getIemType().equals("Doc")){
            docPane.setVisible(true);
            docPane.toFront();
            pio.setText("ความสำคัญ: "+((Document)letter).getPriority());
        }else if (selectedItem.getIemType().equals("Box")){
            boxPane.setVisible(true);
            boxPane.toFront();
            service.setText(((Box)letter).getDeliveryService());
            trackNum.setText(((Box)letter).getTrackingNumber());
        }

    }

    @FXML
    void handleLogoutBtnOnAction(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/loginPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(), 1024, 768));

        stage.show();
    }

    @FXML
    public void handleChangePasswordBtn(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/changePasswordPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(), 1024, 768));

        ChangePasswordController changePasswordController = loader.getController();
        changePasswordController.setAccountManage(account);
        stage.show();
    }
    @FXML public void handleBringReceiveBtn(ActionEvent event)throws IOException{
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/userReceiveInterface.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(), 1024, 768));

        UserReceivedController userReceivedController = loader.getController();
        userReceivedController.setAccountManage(account);
        stage.show();
    }

    public void setAccountManage(Account account) {
        this.account = account;
    }
}
