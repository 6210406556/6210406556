package mailbox.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class AllRoomController {
    @FXML private Button BackBtn;
    @FXML private Button addBtn;
    @FXML private Button editBtn;

    @FXML public void handleBackBthOnAction(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/staffPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(),1024,768));

        stage.show();
    }
    @FXML public void handleAddBthOnAction(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/addRoomPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(),1024,768));

        stage.show();
    }
    @FXML public void handleEditBthOnAction(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/editRoomPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(),1024,768));

        stage.show();
    }
}
