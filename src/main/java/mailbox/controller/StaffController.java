package mailbox.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import mailbox.model.accModel.Account;
import mailbox.model.building.Building;
import mailbox.model.building.Room;
import mailbox.model.storageModel.Box;
import mailbox.model.storageModel.Document;
import mailbox.model.storageModel.Letter;
import mailbox.model.storageModel.Storage;
import mailbox.service.ItemFileDataSource;
import mailbox.service.RoomFileDataSource;
import mailbox.service.StringConfiguration;
import mailbox.strategy.ByRoomNumSearcher;
import mailbox.strategy.SearchNameByRoomSearcher;
import mailbox.strategy.Searcher;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

public class StaffController {
    @FXML
    private Button logoutBtn, takeBtn;
    @FXML
    private Button changePasswordBtn;
    @FXML
    private Button addItemBtn;
    @FXML
    private Button addResidentBtn;
    @FXML
    private Button addRoomBtn;
    @FXML
    private Button roomBtn, searchRoomBtn, searchNameBtn;
    @FXML
    private Button mailboxBtn, viewHistoryBtn;
    @FXML
    private Button okBtnLetter, cancelBtn1, cancelBtn2, cancelBtn3;
    @FXML
    private Button okBtnResident, addInMailBtn, addInDocBtn, addInBoxBtn, addInAddRoomBtn, addInResident;
    @FXML
    private Button okBtnRoom, addMailBtn, addDocBtn, addBoxBtn;
    @FXML
    private Pane paneAddItem, paneAddRoom, paneRoom, paneMailbox, paneAddResident, backPane, addMail, addDoc, addBox, docPane, boxPane;
    @FXML
    private TableView mailboxTableView, roomTableView;
    @FXML
    private Label currentLabel, itemLabel, senderLabel, titleLabel, timeLabel, pio, trackNum, service;
    @FXML
    private ImageView itemPic, profilePic, addItemPic1, addItemPic2, addItemPic3;
    @FXML
    private TextField name1, roomNumber1, size1, senderName1, title1;
    @FXML
    private TextField name2, roomNumber2, size2, senderName2, title2, searchText, searchName;
    @FXML
    private TextField name3, roomNumber3, size3, senderName3, trackingNumber, title3;
    @FXML
    private TextField rooNumber4;
    @FXML
    private TextField roomNumber5, name5;
    @FXML
    private ComboBox<String> priorityComboBox, roomTypeComboBox, serviceComoBoBox, floorComBoBox, floorComBoBox2, towerComBoBox, towerComBoBox2, roomList, towerComBoBox3, towerComBoBox4, towerComBoBox5;
    @FXML
    private ChoiceBox<String> searchOptions;
    @FXML
    private ChoiceBox<String> searchOptionsRoom;
    private ObservableList<Letter> mailboxObservableList;
    private ObservableList<Room> searchRoomObservableList;
    private ObservableList<Room> roomObservableList;
    private Account account;
    private Letter letter;
    private Letter selectedItem;
    private Storage storage;
    private ItemFileDataSource itemFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private Building building;
    private String imagePath;
    private String check = "none";
    private String checkRoom = "none";
    private Searcher searcher;

    @FXML
    public void initialize() {
        itemFileDataSource = new ItemFileDataSource("data");
        storage = itemFileDataSource.getStorageData("Storage.csv");
        roomFileDataSource = new RoomFileDataSource("data");
        building = roomFileDataSource.getRoomData("Room.csv");
        priorityComboBox.getItems().addAll("normal", "high", "highest");
        roomTypeComboBox.getItems().addAll("Double", "Single");
        serviceComoBoBox.getItems().addAll("Thailand Post", "SCG EXPRESS", "Alpha Fast", "Lalamove", "Kerry Express", "Lineman", "DHL Express", "others");
        floorComBoBox.getItems().addAll("1", "2", "3", "4", "5", "6", "7", "8", "9");
        floorComBoBox2.getItems().addAll("1", "2", "3", "4", "5", "6", "7", "8", "9");
        roomList.getItems().addAll("01", "02", "03", "04", "05", "06", "07", "08", "09", "10");
        towerComBoBox.getItems().addAll("A");
        towerComBoBox2.getItems().addAll("A");
        towerComBoBox3.getItems().addAll("A");
        towerComBoBox4.getItems().addAll("A");
        towerComBoBox5.getItems().addAll("A");
        initSearchOption();
        initSearchOptionRoom();
        searchText.setDisable(true);
        searchName.setDisable(true);
        docPane.setVisible(false);
        boxPane.setVisible(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                showMailboxData();
                showRoomData();
                currentLabel.setText("Name:" + account.getAccName());
                profilePic.setImage(new Image(new File(account.getFilePath()).toURI().toString()));
                mailboxTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        showData((Letter) newValue);

                    }
                });

            }
        });
    }

    Date date = new Date();
    SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
    String strDate = formatter.format(date);

    private void initSearchOption() {
        String[] options = {
                "หาจากห้อง",
                "--Clear--"
        };

        for (int i = 0; i < options.length; i++) {
            searchOptions.getItems().add(options[i]);
        }
        searchOptions.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println(newValue);
            Comparator<Letter> sorter = null;
            if (newValue.equals(options[0])) {
                optionByRoomNumSearcher();
            } else if (newValue.equals(options[1])) {
                optionClear();
            }

        });
    }

    private void initSearchOptionRoom() {
        String[] options2 = {
                "หาจากชื่อ",
                "--Clear--"
        };

        for (int i = 0; i < options2.length; i++) {
            searchOptionsRoom.getItems().add(options2[i]);
        }

        searchOptionsRoom.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println(newValue);
            if (newValue.equals(options2[0])) {
                optionSearchByNameSearcher();
            } else if (newValue.equals(options2[1])) {
                optionClearRoom();
            }

        });
    }

    private void optionByRoomNumSearcher() {
        searcher = new ByRoomNumSearcher();
        searchText.setDisable(false);
        searchText.textProperty().addListener((observable, oldValue, newValue) -> {
            String roomNum = newValue;
            ((ByRoomNumSearcher) searcher).setRoomID(roomNum);
        });
    }

    private void optionClear() {
        searchText.setDisable(true);
        searchText.clear();
        searcher = null;
        updateListView(storage.toNotReceiveList());
    }

    private void optionSearchByNameSearcher() {
        searcher = new SearchNameByRoomSearcher();
        searchName.setDisable(false);
        searchName.textProperty().addListener(((observable, oldValue, newValue) -> {
            String name = newValue;
            ((SearchNameByRoomSearcher) searcher).setNameSearch(name);
        }));
    }

    private void optionClearRoom() {
        searchName.setDisable(true);
        searchName.clear();
        searcher = null;
        updateListViewRoom(building.toList());
    }


    private void showRoomData() {
        roomObservableList = FXCollections.observableArrayList(building.toList());
        roomTableView.setItems(roomObservableList);
        ArrayList<StringConfiguration> config = new ArrayList<>();
        config.add(new StringConfiguration("title:อาคาร", "field:tower"));
        config.add(new StringConfiguration("title:หมายเลขห้อง", "field:roomNum"));
        config.add(new StringConfiguration("title:ชั้น", "field:floor"));
        config.add(new StringConfiguration("title:ประเภทห้อง", "field:roomType"));
        config.add(new StringConfiguration("title:ผู้พักอาศัย", "field:residentName"));
        config.add(new StringConfiguration("title:ผู้พักอาศัย", "field:secondResidentName"));


        for (StringConfiguration conf : config) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            col.prefWidthProperty().bind(mailboxTableView.widthProperty().divide(6));
            roomTableView.getColumns().add(col);
        }
    }

    private void showMailboxData() {
        mailboxObservableList = FXCollections.observableArrayList(storage.toNotReceiveList());
        mailboxTableView.setItems(mailboxObservableList);
        ArrayList<StringConfiguration> config = new ArrayList<>();
        config.add(new StringConfiguration("title:ประเภท", "field:iemType"));
        config.add(new StringConfiguration("title:หมายเลขห้อง", "field:roomNumber"));
        config.add(new StringConfiguration("title:หัวข้อ", "field:title"));
        config.add(new StringConfiguration("title:จาก", "field:senderName"));
        config.add(new StringConfiguration("title:ถึง", "field:receiver"));
        config.add(new StringConfiguration("title:วันที่จดหมายเข้าส่วนกลาง", "field:date"));


        for (StringConfiguration conf : config) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            col.prefWidthProperty().bind(mailboxTableView.widthProperty().divide(6));
            mailboxTableView.getColumns().add(col);
        }
    }

    private void showData(Letter letter) {
        selectedItem = letter;
        titleLabel.setText("หัวข้อ: " + letter.getTitle());
        timeLabel.setText("เวลาที่เข้าส่วนกลาง: " + "\n" + letter.getDate());
        itemLabel.setText("ชื่อผู้รับ: " + letter.getReceiver());
        senderLabel.setText("จาก: " + letter.getSenderName());
        if (selectedItem.getPic().equals(null)) {
            itemPic.setImage(new Image("/pic/images.png"));
        }
        itemPic.setImage(new Image(new File(selectedItem.getPic()).toURI().toString()));

        if (selectedItem.getIemType().equals("Doc")) {
            docPane.setVisible(true);
            docPane.toFront();
            pio.setText("ความสำคัญ: " + ((Document) letter).getPriority());
        } else if (selectedItem.getIemType().equals("Box")) {
            boxPane.setVisible(true);
            boxPane.toFront();
            service.setText(((Box) letter).getDeliveryService());
            trackNum.setText(((Box) letter).getTrackingNumber());
        }
    }


    @FXML
    void handleMailboxBtnOnAction(ActionEvent event) throws IOException {
        paneMailbox.toFront();
        backPane.toBack();


    }

    @FXML
    void handleAddItemBtnOnAction(ActionEvent event) throws IOException {
        paneAddItem.toFront();
        backPane.toFront();

    }

    @FXML
    void handleRoomBtnOnAction(ActionEvent event) throws IOException {
        paneRoom.toFront();
        backPane.toFront();

    }

    @FXML
    void handleAddRoomBtnOnAction() {
        paneAddRoom.toFront();
        backPane.toFront();
        floorComBoBox.setValue(null);
        roomTypeComboBox.setValue(null);
        roomList.setValue(null);
        towerComBoBox.setValue(null);

    }

    @FXML
    void handleAddResidentBtnOnAction(ActionEvent event) throws IOException {
        paneAddResident.toFront();
        backPane.toFront();
        roomNumber5.clear();
        name5.clear();
        towerComBoBox2.setValue(null);
        floorComBoBox2.setValue(null);
    }

    @FXML
    void handleAddMailBtnOnAction() {
        addMail.toFront();
        name1.clear();
        roomNumber1.clear();
        senderName1.clear();
        title1.clear();
        size1.clear();
        backPane.toFront();
        addItemPic1.setImage(null);
        towerComBoBox3.setValue(null);
    }

    @FXML
    void handleAddBoxBtnOnAction() {
        addBox.toFront();
        backPane.toFront();
        name3.clear();
        roomNumber3.clear();
        senderName3.clear();
        title3.clear();
        size3.clear();
        serviceComoBoBox.setValue(null);
        trackingNumber.clear();
        addItemPic3.setImage(null);
        towerComBoBox4.setValue(null);
    }

    @FXML
    void handleAddDocBtnOnAction() {
        addDoc.toFront();
        backPane.toFront();
        name2.clear();
        roomNumber2.clear();
        senderName2.clear();
        title2.clear();
        size2.clear();
        priorityComboBox.setValue(null);
        addItemPic2.setImage(null);
        towerComBoBox5.setValue(null);
    }


    @FXML
    public void handleLogoutBtnOnAction(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/loginPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(), 1024, 768));

        stage.show();
    }

    @FXML
    public void handleChangePasswordBtn(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/changePasswordPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(), 1024, 768));

        ChangePasswordController changePasswordController = loader.getController();
        changePasswordController.setAccountManage(account);
        stage.show();
    }

    @FXML
    public void handleViewHistoryBtn(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/viewHistory.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(), 1024, 768));

        ViewHistoryController viewHistoryController = loader.getController();
        viewHistoryController.setAccountManage(account);
        stage.show();
    }

    @FXML
    public void handleAddInMailBtnOnAction(ActionEvent event) throws IOException {
        for (Room room : building.toList()) {
            if (room.getRoomNum().equals(roomNumber1.getText()) && name1.getText().equals("") || senderName1.getText().equals("")
                    || title1.getText().equals("") || size1.getText().equals("") || towerComBoBox3.getSelectionModel().getSelectedIndex() == -1) {
                check = "not";
                break;
            }
            if (roomNumber1.getText().equals(room.getRoomNum())&&check!="not") {
                check = "already";
                break;
            }
        }
        if (check.equals("already")) {

            Letter letter1 = new Letter("Letter", name1.getText(), roomNumber1.getText(), senderName1.getText(),
                    account.getAccName(), title1.getText(), strDate, imagePath, size1.getText(), "nr", "-", "-");
            storage.addItem(letter1);
            mailboxTableView.refresh();
            itemFileDataSource.setLetterData(storage);
            updateListView(storage.toNotReceiveList());
            paneMailbox.toFront();
            backPane.toBack();
            name1.clear();
            roomNumber1.clear();
            senderName1.clear();
            title1.clear();
            size1.clear();
            towerComBoBox3.setValue(null);

        } else if (name1.getText().equals("") || roomNumber1.getText().equals("") || senderName1.getText().equals("")
                || title1.getText().equals("") || size1.getText().equals("") || towerComBoBox3.getSelectionModel().getSelectedIndex() == -1||check.equals("not")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Wrong");
            alert.setContentText("กรุณากรอกข้อมูลให้ครบ");
            alert.setHeaderText(null);
            alert.showAndWait();
        } else if (check.equals("none")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Wrong");
            alert.setContentText("ไม่พบข้อมูล");
            alert.setHeaderText(null);
            alert.showAndWait();

        }

        check = "none";
    }

    @FXML
    public void handleAddInAddRoomOnAction() {

        for (Room room : building.toList()) {
            if (room.getRoomNum().equals(floorComBoBox.getValue() + roomList.getValue())) {
                checkRoom = "already";
                break;
            }
        }
        if (roomList.getValue() == null || floorComBoBox.getValue() == null
                || roomTypeComboBox.getValue() == null || towerComBoBox.getValue() == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Wrong");
            alert.setContentText("กรุณากรอกข้อมูลให้ครบ");
            alert.setHeaderText(null);
            alert.showAndWait();
            checkRoom = "null";
        } else if (checkRoom.equals("already")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Wrong");
            alert.setContentText("มีหมายเลขห้องนี้อยู่แล้วในระบบ");
            alert.setHeaderText(null);
            alert.showAndWait();
        }
        if (checkRoom.equals("none")) {
            Room newRoom = new Room(towerComBoBox.getValue(), floorComBoBox.getValue() + roomList.getValue(), floorComBoBox.getValue(), roomTypeComboBox.getValue(), "-", "-");
            building.addRoom(newRoom);
            roomFileDataSource.setRoomData(building);
            updateListViewRoom(building.toList());
            paneRoom.toFront();
            backPane.toFront();
            roomList.setValue(null);
            towerComBoBox.setValue(null);
            floorComBoBox.setValue(null);
            roomTypeComboBox.setValue(null);
        }
        checkRoom = "none";
    }


    @FXML
    public void handleAddInDocBtnOnAction(ActionEvent event) throws IOException {
        for (Room room : building.toList()) {
            if (room.getRoomNum().equals(roomNumber2.getText()) && name2.getText().equals("") || senderName2.getText().equals("")
                    || title2.getText().equals("") || size2.getText().equals("") || priorityComboBox.getSelectionModel().getSelectedIndex() == -1
                    || towerComBoBox5.getSelectionModel().getSelectedIndex() == -1) {
                check = "not";
                break;
            }
            if (roomNumber2.getText().equals(room.getRoomNum()) && !check.equals("not")) {
                check = "already";
                break;
            }
        }
        if (check.equals("already")) {

            Document document = new Document("Doc", name2.getText(), roomNumber2.getText(), senderName2.getText(),
                    account.getAccName(), title2.getText(), strDate, imagePath, size2.getText(), "nr", "-", "-", priorityComboBox.getValue());
            storage.addItem(document);
            mailboxTableView.refresh();
            itemFileDataSource.setLetterData(storage);
            updateListView(storage.toNotReceiveList());
            paneMailbox.toFront();
            backPane.toBack();
            name2.clear();
            roomNumber2.clear();
            senderName2.clear();
            title2.clear();
            size2.clear();
            priorityComboBox.setValue(null);
            towerComBoBox5.setValue(null);
        } else if (name2.getText().equals("") || roomNumber2.getText().equals("") || senderName2.getText().equals("")
                || title2.getText().equals("") || size2.getText().equals("") || priorityComboBox.getSelectionModel().getSelectedIndex() == -1
                || towerComBoBox5.getSelectionModel().getSelectedIndex() == -1 || check.equals("not")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Wrong");
            alert.setContentText("กรุณากรอกข้อมูลให้ครบ");
            alert.setHeaderText(null);
            alert.showAndWait();
        } else if (check.equals("none")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Wrong");
            alert.setContentText("ไม่พบข้อมูล");
            alert.setHeaderText(null);
            alert.showAndWait();

        }

        check = "none";

    }

    @FXML
    public void handleAddInBoxBtnOnAction(ActionEvent event) throws IOException {
        for (Room room : building.toList()) {
            if (room.getRoomNum().equals(roomNumber3.getText()) && name3.getText().equals("") || senderName3.getText().equals("")
                    || title3.getText().equals("") || size3.getText().equals("") || serviceComoBoBox.getSelectionModel().getSelectedIndex() == -1
                    || trackingNumber.equals("") || towerComBoBox4.getSelectionModel().getSelectedIndex() == -1) {
                check = "not";
                break;
            }
            if (roomNumber3.getText().equals(room.getRoomNum()) && !check.equals("not")) {
                check = "already";
                break;
            }
        }
        if (check.equals("already")) {

            Box box = new Box("Box", name3.getText(), roomNumber3.getText(), senderName3.getText(),
                    account.getAccName(), title3.getText(), strDate, imagePath, size3.getText()
                    , "nr", "-", "-", serviceComoBoBox.getValue(), trackingNumber.getText());
            storage.addItem(box);
            mailboxTableView.refresh();
            itemFileDataSource.setLetterData(storage);
            updateListView(storage.toNotReceiveList());
            paneMailbox.toFront();
            backPane.toBack();
            name3.clear();
            roomNumber3.clear();
            senderName3.clear();
            title3.clear();
            size3.clear();
            serviceComoBoBox.setValue(null);
            trackingNumber.clear();
            towerComBoBox4.setValue(null);
        } else if (name3.getText().equals("") || roomNumber3.getText().equals("") || senderName3.getText().equals("")
                || title3.getText().equals("") || size3.getText().equals("") || serviceComoBoBox.getSelectionModel().getSelectedIndex() == -1
                || trackingNumber.equals("") || towerComBoBox4.getSelectionModel().getSelectedIndex() == -1 || check.equals("not")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Wrong");
            alert.setContentText("กรุณากรอกข้อมูลให้ครบ");
            alert.setHeaderText(null);
            alert.showAndWait();
        } else if (check.equals("none")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Wrong");
            alert.setContentText("ไม่พบข้อมูล");
            alert.setHeaderText(null);
            alert.showAndWait();

        }

        check = "none";

    }

    @FXML
    public void handleCancelBtnOnAction(ActionEvent event) throws IOException {
        paneAddItem.toFront();
        backPane.toFront();

    }

    @FXML
    public void handleAddBtnOnAction(ActionEvent event) throws IOException {
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("images PNG JPG", "*.png", "*.jpg"));
        File file = chooser.showOpenDialog(profilePic.getScene().getWindow());
        if (file != null) {
            try {
                File destDir = new File("images");
                destDir.mkdirs();
                String[] fileSplit = file.getName().split(File.separator + "\\.");
                String filename = LocalDate.now() + "_" + System.currentTimeMillis() + "_" + fileSplit[fileSplit.length - 1];
                String imgFile = "images" + File.separator + filename;
                Files.copy(file.toPath(), new File(imgFile).toPath(), StandardCopyOption.REPLACE_EXISTING);
                addItemPic1.setImage(new Image(new File(imgFile).toURI().toString()));
                addItemPic2.setImage(new Image(new File(imgFile).toURI().toString()));
                addItemPic3.setImage(new Image(new File(imgFile).toURI().toString()));
                this.imagePath = imgFile;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @FXML
    public void AddInResident(ActionEvent event) throws IOException {

        for (Room room : building.toList()) {
            if (room.getRoomNum().equals(roomNumber5.getText()) && (name5.getText().equals("") || towerComBoBox2.getSelectionModel().getSelectedIndex() == -1
                    || floorComBoBox2.getSelectionModel().getSelectedIndex() == -1)) {
                checkRoom = "not";
                break;
            }
            if (room.getRoomNum().equals(roomNumber5.getText()) && towerComBoBox2.getValue().equals(room.getTower()) && floorComBoBox2.getValue().equals(room.getFloor())) {
                if (room.getResidentName().equals("-")) {
                    room.setResidentName(name5.getText());
                    checkRoom = "already";
                    roomFileDataSource.setRoomData(building);
                    updateListViewRoom(building.toList());
                    paneRoom.toFront();
                    backPane.toFront();
                    break;
                } else if (room.getResidentName() != "-" && room.getSecondResidentName().equals("-") && room.getRoomType().equals("Double")) {
                    room.setSecondResidentName(name5.getText());
                    checkRoom = "already";
                    roomFileDataSource.setRoomData(building);
                    updateListViewRoom(building.toList());
                    paneRoom.toFront();
                    backPane.toFront();
                    break;
                } else {
                    Alert a = new Alert(Alert.AlertType.WARNING);
                    a.setTitle("Wrong");
                    a.setContentText("ห้องเต็ม");
                    a.setHeaderText(null);
                    a.showAndWait();
                    checkRoom = "already";
                    break;
                }
            }

        }
        if (roomNumber5.getText().equals("") || name5.getText().equals("") || towerComBoBox2.getSelectionModel().getSelectedIndex() == -1
                || floorComBoBox2.getSelectionModel().getSelectedIndex() == -1 || (checkRoom.equals("not"))) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Wrong");
            alert.setContentText("กรุณากรอกข้อมูลให้ครบ");
            alert.setHeaderText(null);
            alert.showAndWait();
        } else if (checkRoom == "none") {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Wrong");
            alert.setContentText("กรอกข้อมูลผิดพลาด");
            alert.setHeaderText(null);
            alert.showAndWait();
        }
        checkRoom = "none";
    }

    @FXML
    public void handleTakeBtnOnAction(ActionEvent event) {
        if (selectedItem.getStatus().equals("nr")) {
            selectedItem.setStatus("r");
            selectedItem.setTakenDate(strDate);
            selectedItem.setStaffTake(account.getName());
        }

        updateListView(storage.toNotReceiveList());
        mailboxTableView.getSelectionModel().clearSelection();
        itemFileDataSource.setLetterData(storage);

    }

    private void updateListView(ArrayList<Letter> list) {
        mailboxTableView.getItems().clear();
        for (Letter letter : list) {
            mailboxTableView.getItems().add(letter);
        }
    }

    @FXML
    public void handleUpdateButton(ActionEvent event) {
        if (searcher != null) {
            updateListView(storage.search(searcher));
        } else {
            updateListView(storage.toNotReceiveList());
        }
    }

    private void updateListViewRoom(ArrayList<Room> list) {
        roomTableView.getItems().clear();
        for (Room room : list) {
            roomTableView.getItems().add(room);
        }
    }

    @FXML
    public void handleUpdateButtonRoom(ActionEvent event) {
        if (searcher != null) {
            updateListViewRoom(building.search(searcher));
        } else {
            updateListViewRoom(building.toList());
        }
    }

    public void setAccountManage(Account account) {
        this.account = account;
    }

    public ObservableList<Letter> getMailboxObservableList() {
        return mailboxObservableList;
    }
}
