package mailbox.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import mailbox.model.accModel.Account;
import mailbox.model.accModel.AccountManage;
import mailbox.service.UserFileDataSource;


import java.io.IOException;

public class LoginController {

    private UserFileDataSource dataSource;
    private Account account;
    @FXML private PasswordField passwordField;
    @FXML private TextField textField;
    @FXML private Button loginBtn;
    @FXML private Button registerBtn;
    @FXML private Button creditBtn;
    private AccountManage accountManage;
    private String checkState = "cantlog";
    @FXML public void initialize(){
        Platform.runLater(()->{
            dataSource = new UserFileDataSource("data");
            accountManage = new AccountManage();
            accountManage = dataSource.getAccountsData("User.csv");
        });
    }

    @FXML public void handleLoginBtnOnAction(ActionEvent event) throws IOException {
        for (Account account : accountManage.toList()) {
            if (accountManage.checkPassword(textField.getText(),passwordField.getText())) {
                if (accountManage.getCurrentAccount().getAccStatus().equals("ban")){
                    checkState = "ban";
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Wrong");
                    alert.setContentText("ถูกแบนออกจากระบบ");
                    alert.setHeaderText(null);
                    alert.showAndWait();
                    accountManage.getCurrentAccount().setAttempt(accountManage.getCurrentAccount().getAttempt()+1);
                    dataSource.setStaffData(accountManage);
                    break;
                }
                if (accountManage.getCurrentAccount().isAdmin()) {
                    Button login = (Button) event.getSource();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/adminInterface.fxml"));
                    Stage stage = (Stage) login.getScene().getWindow();
                    stage.setScene(new Scene(loader.load(), 1024, 768));
                    AdminController adminController= loader.getController();
                    adminController.setAccountManage(accountManage.getCurrentAccount());

                    stage.show();
                    checkState = "login";
                    break;
                }  if (accountManage.getCurrentAccount().isStaff()) {
                    accountManage.getCurrentAccount().setDate();
                    dataSource.setStaffData(accountManage);
                    Button register = (Button) event.getSource();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/staffPage.fxml"));
                    Stage stage = (Stage) register.getScene().getWindow();
                    stage.setScene(new Scene(loader.load(), 1024, 768));
                    StaffController officeController= loader.getController();
                    officeController.setAccountManage(accountManage.getCurrentAccount());
                    stage.show();
                    checkState = "login";
                    break;
                }  if (accountManage.getCurrentAccount().isU()) {
                    Button register = (Button) event.getSource();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/userInterface.fxml"));
                    Stage stage = (Stage) register.getScene().getWindow();
                    stage.setScene(new Scene(loader.load(), 1024, 768));
                    UserController userController = loader.getController();
                    userController.setAccountManage(accountManage.getCurrentAccount());
                    stage.show();
                    checkState = "login";
                    break;
                }

            }
        }
        if (checkState.equals("cantlog")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Wrong");
            alert.setContentText("user name หรือ รหัสผ่าน ผิดกรุณาลองใหม่");
            alert.setHeaderText(null);
            alert.showAndWait();
        }
        checkState = "cantlog";
    }


    @FXML public void handleRegisterBtnOnAction(ActionEvent event) throws IOException{
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/registerPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(),1024,768));

        stage.show();
    }
    @FXML public void handleCreditBtnOnAction(ActionEvent event) throws IOException{
        Button help = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/creditPage.fxml"));
        Stage stage = (Stage) help.getScene().getWindow();
        stage.setScene(new Scene(loader.load(),1024,768));


        stage.show();
    }
    @FXML public void handleHelpBtnOnAction(ActionEvent event) throws IOException{
        Button help = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/helpPage.fxml"));
        Stage stage = (Stage) help.getScene().getWindow();
        stage.setScene(new Scene(loader.load(),1024,768));

        stage.show();
    }






}

