package mailbox.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import mailbox.model.accModel.Account;
import mailbox.model.accModel.AccountManage;
import mailbox.service.UserFileDataSource;


import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;





public class AddStaffController {
    @FXML private TextField userName;
    @FXML private PasswordField passwordField;
    @FXML private TextField name;
    @FXML private Button submitBtn;
    @FXML private Button backBtn;
    @FXML private Button addBtn;
    @FXML private Pane pane;
    private AccountManage accountManage;
    private UserFileDataSource dataSource;
    private Account account;
    private String checkUser = "none";
    private String imagePath ;
    @FXML private ImageView profilePic;
    @FXML public void initialize() {
        Platform.runLater(() -> {
            dataSource = new UserFileDataSource("data");
            accountManage = new AccountManage();
            accountManage = dataSource.getAccountsData("User.csv");
        });
    }
    @FXML public void handleAddBtnOnAction(ActionEvent event) throws  IOException{
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("images PNG JPG", "*.png", "*.jpg"));
        File file = chooser.showOpenDialog(profilePic.getScene().getWindow());
        if (file != null){
            try {
                File destDir = new File("images");
                destDir.mkdirs();
                String[] fileSplit = file.getName().split(File.separator + "\\.");
                String filename = LocalDate.now()+"_"+System.currentTimeMillis()+"_"+fileSplit[fileSplit.length - 1];
                String imgFile = "images" + File.separator + filename;
                Files.copy(file.toPath(),new File(imgFile).toPath(), StandardCopyOption.REPLACE_EXISTING );
                profilePic.setImage(new Image(new File(imgFile).toURI().toString()));
                this.imagePath = imgFile;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    @FXML public void handleSubmitBtnOnAction(ActionEvent event) throws  IOException{

        for (Account account:accountManage.toList()){
            if (userName.getText().equals(account.getAccName())){
                checkUser = "already";
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Wrong");
                alert.setContentText("usernameถูกใช้ไปแล้ว");
                alert.setHeaderText(null);
                alert.showAndWait();
                break;
            }
        }
        if (checkUser.equals("none")){
            Account acc = new Account(userName.getText(),passwordField.getText(),name.getText(),0,"-",imagePath,"s","-");
            accountManage.addAccount(acc);
            dataSource.setStaffData(accountManage);
            Button back = (Button) event.getSource();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/adminInterface.fxml"));
            Stage stage = (Stage) back.getScene().getWindow();
            stage.setScene(new Scene(loader.load(),1024,768));
            AdminController adminController= loader.getController();
            adminController.setAccountManage(account);
            stage.show();
        }
        else if (userName.getText().equals("")||passwordField.getText().equals("")||name.getText().equals("")){
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("Wrong");
            a.setContentText("กรุณากรอกข้อมูลให้ครบ");
            a.setHeaderText(null);
            a.showAndWait();
        }
        checkUser = "none";
    }
    @FXML public void handleBackBtnOnAction(ActionEvent event) throws IOException{

        Button back = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/adminInterface.fxml"));
        Stage stage = (Stage) back.getScene().getWindow();
        stage.setScene(new Scene(loader.load(),1024,768));
        AdminController adminController= loader.getController();
        adminController.setAccountManage(account);

    }


    public void setAccountManage(Account account) {this.account = account;
    }
}
