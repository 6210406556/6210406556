package mailbox.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import mailbox.model.accModel.Account;
import mailbox.model.accModel.AccountManage;
import mailbox.model.building.Building;
import mailbox.model.building.Room;
import mailbox.service.RoomFileDataSource;
import mailbox.service.UserFileDataSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;

public class RegisterController {
    private AccountManage accountManage;
    private UserFileDataSource dataSource;
    private RoomFileDataSource roomFileDataSource;
    private Building building;
    private String imagePath;
    private String checkUser = "none";
    private String checkData = "none";
    @FXML
    private Button submitBtn;
    @FXML
    private TextField userName;
    @FXML
    private PasswordField passwordField;
    @FXML
    private TextField name;
    @FXML
    private TextField roomNum;
    @FXML
    private ImageView profilePic;
    @FXML
    private Button addBtn;
    @FXML
    private Button backBtn;

    @FXML
    public void initialize() {
        Platform.runLater(() -> {
            dataSource = new UserFileDataSource("data");
            accountManage = new AccountManage();
            accountManage = dataSource.getAccountsData("User.csv");
            roomFileDataSource = new RoomFileDataSource("data");
            building = roomFileDataSource.getRoomData("Room.csv");

        });
    }

    @FXML
    public void handleBackBthOnAction(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/loginPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(), 1024, 768));

        stage.show();
    }

    @FXML
    public void handleAddBtnOnAction(ActionEvent event) throws IOException {
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("images PNG JPG", "*.png", "*.jpg"));
        File file = chooser.showOpenDialog(profilePic.getScene().getWindow());
        if (file != null){
            try {
                File destDir = new File("images");
                destDir.mkdirs();
                String[] fileSplit = file.getName().split(File.separator + "\\.");
                String filename = LocalDate.now()+"_"+System.currentTimeMillis()+"_"+fileSplit[fileSplit.length - 1];
                String imgFile = "images" + File.separator + filename;
                Files.copy(file.toPath(),new File(imgFile).toPath(), StandardCopyOption.REPLACE_EXISTING );
                profilePic.setImage(new Image(new File(imgFile).toURI().toString()));
                this.imagePath = imgFile;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @FXML
    public void handleSubmitBtnOnAction(ActionEvent event) throws IOException {
        for (Room room : building.toList()) {
            if (roomNum.getText().equals("") || name.getText().equals("") || passwordField.getText().equals("")) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("ผิดพลาด");
                alert.setContentText("กรุณากรอกข้อมูลให้ครบถ้วน");
                alert.setHeaderText(null);
                alert.showAndWait();
                break;
            } else if (roomNum.getText().equals(room.getRoomNum()) && (name.getText().equals(room.getResidentName())||name.getText().equals(room.getSecondResidentName()))) {
                checkData = "correct";
                break;
            }
        }
        if (checkData.equals("none")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("ผิดพลาด");
            alert.setContentText("ข้อมูลไม่ตรงกับในระบบ");
            alert.setHeaderText(null);
            alert.showAndWait();
            checkData = "incorrect";
        }
        else if (checkData.equals("correct")) {
            for (Account acc : accountManage.toList()) {
                if (userName.getText().equals(acc.getAccName())) {
                    checkUser = "already";
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("ผิดพลาด");
                    alert.setContentText("ชื่อผู้ใช้งานถูกใช้ไปแล้ว");
                    alert.setHeaderText(null);
                    alert.showAndWait();
                    break;
                }else if(name.getText().equals(acc.getName())){}
            }

            if (checkUser.equals("none")) {
                Account account = new Account(userName.getText(), passwordField.getText(), name.getText(), 0, "-", imagePath, "u","-");
                account.setRoomNumber(roomNum.getText());
                accountManage.addAccount(account);
                dataSource.setStaffData(accountManage);
                Button register = (Button) event.getSource();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/loginPage.fxml"));
                Stage stage = (Stage) register.getScene().getWindow();
                stage.setScene(new Scene(loader.load(), 1024, 768));

                stage.show();
            }

        }
        checkUser = "none";
        checkData = "none";

    }


}
