package mailbox.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import mailbox.model.accModel.Account;
import mailbox.model.building.Building;
import mailbox.model.storageModel.Letter;
import mailbox.model.storageModel.Storage;
import mailbox.service.ItemFileDataSource;
import mailbox.service.RoomFileDataSource;
import mailbox.service.StringConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class UserReceivedController {
    private Account account;
    private ItemFileDataSource itemFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private Storage storage;
    private Building building;
    @FXML private Label currentLabel;
    @FXML private ImageView profilePic,itemPic;
    @FXML private TableView<Letter> mailboxTableView;
    private ObservableList<Letter> mailboxObservableList;
    private Letter selectedItem;
    @FXML private Label staffReceive,timeLabel,staffTake,takeDate;
    @FXML
    public void initialize() {
        itemFileDataSource = new ItemFileDataSource("data");
        storage = itemFileDataSource.getStorageData("Storage.csv");
        roomFileDataSource = new RoomFileDataSource("data");
        building = roomFileDataSource.getRoomData("Room.csv");

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                showMailboxData();
                currentLabel.setText("Name:" + account.getAccName());
                profilePic.setImage(new Image(new File(account.getFilePath()).toURI().toString()));
                mailboxTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        showData(newValue);

                    }
                });

            }
        });
    }
    private void showMailboxData() {
        ArrayList<Letter> newList = new ArrayList<>();
        for(Letter letter:storage.toReceiveList()){
            if (account.getRoomNumber().equals(letter.getRoomNumber())){
                newList.add(letter);
            }
        }
        mailboxObservableList = FXCollections.observableArrayList(newList);
        mailboxTableView.setItems(mailboxObservableList);
        ArrayList<StringConfiguration> config = new ArrayList<>();
        config.add(new StringConfiguration("title:ประเภท", "field:iemType"));
        config.add(new StringConfiguration("title:หัวข้อ", "field:title"));
        config.add(new StringConfiguration("title:ถึง", "field:receiver"));
        config.add(new StringConfiguration("title:หมายเลขห้อง", "field:roomNumber"));
        config.add(new StringConfiguration("title:วันที่จดหมายเข้าส่วนกลาง", "field:date"));
        config.add(new StringConfiguration("title:จาก", "field:senderName"));

        for (StringConfiguration conf : config) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            col.prefWidthProperty().bind(mailboxTableView.widthProperty().divide(6));
            mailboxTableView.getColumns().add(col);
        }
    }


    private void showData(Letter letter) {
        selectedItem = letter;
        staffReceive.setText(letter.getStaffName());
        timeLabel.setText(letter.getDate());
        staffTake.setText(letter.getStaffTake());
        takeDate.setText(letter.getDate());
        itemPic.setImage(new Image(new File(selectedItem.getPic()).toURI().toString()));
    }

    @FXML
    void handleLogoutBtnOnAction(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/loginPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(), 1024, 768));

        stage.show();
    }

    @FXML
    public void handleChangePasswordBtn(ActionEvent event) throws IOException {
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/changePasswordPage.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(), 1024, 768));

        ChangePasswordController changePasswordController = loader.getController();
        changePasswordController.setAccountManage(account);
        stage.show();
    }
    @FXML public void handleStorageBtn(ActionEvent event)throws IOException{
        Button register = (Button) event.getSource();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/userInterface.fxml"));
        Stage stage = (Stage) register.getScene().getWindow();
        stage.setScene(new Scene(loader.load(), 1024, 768));
        UserController userController = loader.getController();
        userController.setAccountManage(account);
        stage.show();
    }
    public void setAccountManage(Account account) {this.account =account;
    }
}
